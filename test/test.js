var expect  = require("chai").expect;
const counterModule = require('../counter');

describe ("reverse polish notation calc test", function(){

  // plus
  describe ("add check", function(){
    it("adds numbers", function() {
      var added  =  counterModule.reverseCounter("3 3 3 + +"),
          added2  =  counterModule.reverseCounter("3 3 3 3 + + +");
      expect(added).to.equal(9);
      expect(added2).to.equal(12);
    });
  });

  // minus
  describe ("minus check", function(){
    it("substracts numbers", function() {
      var minus  =  counterModule.reverseCounter("3 3 -"),
          minus2  =  counterModule.reverseCounter("222 22 -");
      expect(minus).to.equal(0);
      expect(minus2).to.equal(200);
    });
  });

  // multyply
  describe ("multiply check", function(){
    it("multiplies numbers", function() {
      var m  =  counterModule.reverseCounter("3 3 *"),
          m2  =  counterModule.reverseCounter("2 2 2 * *");
      expect(m).to.equal(9);
      expect(m2).to.equal(8);
    });
  });


  // %
  describe ("% check", function(){
    it("checks what's left from the division", function() {
      var u  =  counterModule.reverseCounter("4 4 %");
      expect(u).to.equal(0);
    });
  });

  // complex
  describe ("complex action check", function(){
    it("checks complex strings", function() {
      var c  =  counterModule.reverseCounter("1 2 3 % +");
      expect(c).to.equal(3);
    });
  });

});
