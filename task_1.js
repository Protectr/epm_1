var http = require("http");

const counterModule = require('./counter');
var result = counterModule.reverseCounter("1 2 3 % +");

console.log(result);

http.createServer(function(req, res){
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end(`The answer is ... ${result}`);
}).listen(8000);
