module.exports = {
  //  hello: function(asd) {
  //     return "Hello " + asd;
  //  }

 reverseCounter: function(str){
   var result,
       isNum = /^\d+$/,
       testChar = /^[A-z]+$/, // not an alphabetical
       symbol,
       stack = [],
       convertOperator = {
         "+" : function(x, y) { return x + y },
         "-" : function(x, y) { return x - y },
         "*" : function(x, y) { return x * y },
         "%" : function(x, y) { return x % y }
       },
       array = str.split(" ");
       while (symbol = array.shift()){
         // symbol = +symbol also works for detecting number
         if (isNum.test(symbol)) {
           stack.push(symbol);
         } else {
           if (testChar.test(symbol)) throw new Error('Incorrect input at : <' + symbol + '>');
           let op2 = stack.pop(),
               op1 = stack.pop(),
               res;
           res = convertOperator[symbol](+op1, +op2);
           //console.log(res);
           stack.push(res);
         }
       }
   return stack.pop();
 }

}
